package dk.semlerit.autocore.rs.azure.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.weld.junit5.EnableWeld;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import dk.semlerit.autocore.rs.azure.CalendarService;
import dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport;
import dk.semlerit.autocore.rs.azure.model.CalendarEvent;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@EnableWeld
@ExtendWith(MockitoExtension.class)
class DefaultCalendarServiceTest {

	@WeldSetup
	public WeldInitiator weld = WeldInitiator.from(DefaultCalendarService.class, DefaultAzureClientSupport.class)
			.activate(ApplicationScoped.class).build();

	@Inject
	private CalendarService calendarService;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link dk.semlerit.autocore.rs.azure.impl.impl.DefaultCalendarService#retrieveCalendarEvents(java.lang.String, java.util.Date, java.util.Date)}.
	 */
	@Test
	final void testRetrieveCalendarEvents() {
		Calendar fromCal = Calendar.getInstance();
		fromCal.set(2019, Calendar.MARCH, 7, 8, 0, 0);
		Date fromDate = fromCal.getTime();
		fromCal.add(Calendar.HOUR, 10);
		Date toDate = fromCal.getTime();

		List<CalendarEvent> result = calendarService.retrieveCalendarEvents("thki@semler.dk", fromDate, toDate);

		Assertions.assertNotNull(result);
	}
}
