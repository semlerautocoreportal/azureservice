package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import dk.semlerit.autocore.rs.azure.core.map.AzureDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalendarEvent implements Serializable {
	@JsonIgnore
	private static final long serialVersionUID = -4622535978721522763L;

	@JsonProperty("id")
	private String id;

	@JsonDeserialize(using = AzureDateTime.UTCDateTimeDeserializer.class)
	@JsonProperty("start")
	private Date startDateTime;

	@JsonDeserialize(using = AzureDateTime.UTCDateTimeDeserializer.class)
	@JsonProperty("end")
	private Date endDateTime;

	@JsonProperty("type")
	private String appointmentType;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("body")
	private BodyContent bodyContent;

	@JsonProperty("location")
	private LocationContent locationContent;
}
