package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyContent implements Serializable {
	@JsonIgnore
	private static final long serialVersionUID = -8543226610518006694L;

	@JsonProperty("contentType")
	private String contentType;
	@JsonProperty("content")
	private String content;
}
