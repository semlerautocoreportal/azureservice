package dk.semlerit.autocore.rs.azure;

import java.util.Date;
import java.util.List;

import javax.ws.rs.ClientErrorException;

import dk.semlerit.autocore.rs.azure.model.CalendarEvent;

/**
 * General service for accessing calendar oriented office services.
 * 
 * @author edbbrpe
 * @version 2.0
 * @since 3.0.0
 */
public interface CalendarService {

	/**
	 * Return a list of Azure Calendar events.
	 * 
	 * Method will not return {@literal null}.
	 * 
	 * @param userPrincipalName
	 *            typically the email address of the user.
	 * @param fromDateTime
	 *            beginning of the time span to retrieve events within.
	 * @param toDateTime
	 *            end of the time span to retrieve events within
	 * @return a list of Calendar events or an empty list.
	 * @throws ClientErrorException
	 *             or sub-class in case of error
	 */
	List<CalendarEvent> retrieveCalendarEvents(String userPrincipalName, Date fromDateTime, Date toDateTime)
			throws ClientErrorException;

}
