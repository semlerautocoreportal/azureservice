package dk.semlerit.autocore.rs.azure.impl;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import dk.semlerit.autocore.rs.azure.CalendarService;
import dk.semlerit.autocore.rs.azure.core.AzureClientSupport;
import dk.semlerit.autocore.rs.azure.model.CalendarEvent;
import dk.semlerit.autocore.rs.azure.model.CalendarEventResult;
import lombok.extern.slf4j.Slf4j;

/**
 * Default implementation for Azure Calendar service.
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 * @see CalendarService
 */
@Slf4j
@ApplicationScoped
public class DefaultCalendarService implements CalendarService {
	private static final DateTimeFormatter UTC_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	private static final String GRAPH_RESOURCE = "https://graph.microsoft.com";
	private static final String GRAPH_BASE_URI = String.join("/", GRAPH_RESOURCE, "beta");
	private static final String[] FIELD_SELECTIONS = { "subject", "body", "bodyPreview", "organizer", "attendees",
			"start", "end", "location" };

	@Inject
	private AzureClientSupport azureClientSupport;

	private Client client;

	/**
	 * @throws NullPointerException
	 *             if userPrincipalName, fromDate or toDate is {@literal null}
	 */
	@Override
	public List<CalendarEvent> retrieveCalendarEvents(@NotNull String userPrincipalName, @NotNull Date fromDateTime,
			@NotNull Date toDateTime) throws ClientErrorException {

		final ZonedDateTime zonedFromDateTime = ZonedDateTime.ofInstant(
				Objects.requireNonNull(fromDateTime, "fromDateTime must not be null").toInstant(),
				ZoneOffset.ofOffset("UTC", ZoneOffset.UTC));
		final ZonedDateTime zonedToDateTime = ZonedDateTime.ofInstant(
				Objects.requireNonNull(toDateTime, "toDateTime must not be null").toInstant(),
				ZoneOffset.ofOffset("UTC", ZoneOffset.UTC));

		final URI uri = UriBuilder.fromUri(GRAPH_BASE_URI).path("users")
				.path(Objects.requireNonNull(userPrincipalName, "userPrincipal must not be null")).path("calendarview")
				.queryParam("startdatetime", UTC_FORMATTER.format(zonedFromDateTime))
				.queryParam("enddatetime", UTC_FORMATTER.format(zonedToDateTime))
				.queryParam("$select", String.join(",", FIELD_SELECTIONS)).build();

		Instant start = Instant.now();

		CalendarEventResult calendarEventResult = client.target(uri).request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION,
						String.format("Bearer %s", azureClientSupport.getAccessToken(GRAPH_RESOURCE)))
				.get(CalendarEventResult.class);

		Instant end = Instant.now();
		log.debug("Remote rest call took: {} milliseconds.", Duration.between(start, end));

		return calendarEventResult.getResult();
	}

	@PostConstruct
	public void init() {
		this.client = ClientBuilder.newClient();
	}

	@PreDestroy
	public void destroy() {
		if (Objects.nonNull(client)) {
			client.close();
		}
		client = null;
	}
}
