/**
 * 
 */
package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalendarEventResult implements Serializable {
	@JsonIgnore
	private static final long serialVersionUID = -7174777976423494014L;

	@JsonProperty(value = "value")
	private List<CalendarEvent> result = new ArrayList<>();
}
