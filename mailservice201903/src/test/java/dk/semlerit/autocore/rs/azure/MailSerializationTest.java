package dk.semlerit.autocore.rs.azure;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import dk.semlerit.autocore.rs.azure.model.Attachment;
import dk.semlerit.autocore.rs.azure.model.Body;
import dk.semlerit.autocore.rs.azure.model.ContentType;
import dk.semlerit.autocore.rs.azure.model.DefaultMessage;
import dk.semlerit.autocore.rs.azure.model.FileAttachment;
import dk.semlerit.autocore.rs.azure.model.Importance;
import dk.semlerit.autocore.rs.azure.model.Message;
import dk.semlerit.autocore.rs.azure.model.Recipient;
import dk.semlerit.autocore.rs.azure.model.SendMailMessageDecorator;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
class MailSerializationTest {
	private static final String HTML_BODY = "<html><head><title>Help Kids Code Project</title></head><style type=\"text/css\">@media only screen and (max-width: 480px) {table[class=email], table[class=email-content] { clear: both; width: 320px !important; }}</style><body> <!-- First HTML table acts as a pseudo BODY tag to protect email inside --> <table width=\"99%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td align=\"center\" valign=\"top\" bgcolor=\"#efefef\"> <!-- Second HTML table is the email itself --> <table class=\"email\" width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <p style=\"font-family: Helvetica, Arial, sans-serif; font-size: 10px; line-height: 12px; margin-top: 20px; margin-right: 0; margin-bottom: 20px; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> A short sentence telling readers how you got their email address and where to unsubscribe. </p> </td> </tr> <tr> <td align=\"left\" valign=\"top\" bgcolor=\"#ffffff\"> <h1 style=\"font-family: Georgia, Times, serif; font-size: 48px; font-weight: normal; line-height: 48px; margin-top: 20px; margin-right: 0; margin-bottom: 10px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> 1-2-3 </h1> <p style=\"font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 12px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> <a href=\"\" target=\"_blank\" style=\"font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 12px; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0; text-decoration: none;\"> HOME </a> | <a href=\"\" target=\"_blank\" style=\"font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 12px; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0; text-decoration: none;\"> LINK 1</a> | <a href=\"\" target=\"_blank\" style=\"font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 12px; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0; text-decoration: none;\"> LINK 2</a> | <a href=\"\" target=\"_blank\" style=\"font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 12px; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0; text-decoration: none;\"> LINK 3</a> </p> </td> </tr> <tr> <td valign=\"top\" bgcolor=\"#999999\"> <!-- Third HTML table is the left column of content --> <table class=\"email-content\" align=\"left\" width=\"350\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td valign=\"top\" bgcolor=\"#353535\"> <h2 style=\"color: #efefef; font-family: Helvetica, Arial, sans-serif; font-size: 24px; font-weight: normal; line-height: 24px; margin-top: 20px; margin-right: 0; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> Fusce Interdum </h2> <p style=\"color: #efefef; font-family: Georgia, Times, serif; font-size: 15px; line-height: 22px; margin-top: 0; margin-right: 10px; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> Donec in lacus in ante facilisis dignissim quis auctor elit. Praesent hendrerit ligula sit amet tortor luctus, ut hendrerit turpis fringilla. Maecenas nec sapien lacinia, mollis nisi ut, euismod turpis. </p> <p style=\"color: #efefef; font-family: Georgia, Times, serif; font-size: 15px; line-height: 22px; margin-top: 0; margin-right: 10px; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> Curabitur cursus metus hendrerit iaculis laoreet. Vivamus risus felis, sollicitudin at ipsum eu, placerat laoreet massa. Phasellus eget justo luctus, facilisis urna lobortis, consectetur tellus. Nunc quis euismod purus, dapibus condimentum enim. </p> </td> </tr> </table> <!-- Close third HTML table --> <!-- Fourth HTML table is the right column of content --> <table class=\"email-content\" align=\"right\" width=\"150\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td valign=\"top\" bgcolor=\"#999999\"> <h3 style=\"color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 14px; margin-top: 20px; margin-right: 0; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> Side Content Heading </h3> <p style=\"color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 18px; margin-top: 0; margin-right: 0; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> <a style=\"color: #ffffff; text-decoration: none;\" href=\"\" target=\"_blank\">Link 1</a><br> <a style=\"color: #ffffff; text-decoration: none;\" href=\"\" target=\"_blank\">Link 2</a><br> <a style=\"color: #ffffff; text-decoration: none;\" href=\"\" target=\"_blank\">Link 3</a> </p> </td> </tr> </table> <!-- Close fourth HTML table --> </td> </tr> <tr> <td align=\"left\" valign=\"top\" bgcolor=\"#cccccc\" height=\"100\"> <p style=\"font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 12px; margin-top: 20px; margin-right: 0; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> Footer text and links go here. </p> </td> </tr> <tr> <td align=\"left\" valign=\"top\"> <p style=\"font-family: Helvetica, Arial, sans-serif; font-size: 10px; line-height: 12px; margin-top: 20px; margin-right: 0; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> UNSUBSCRIBE INSTRUCTIONS/LINK </p> <p style=\"font-family: Helvetica, Arial, sans-serif; font-size: 10px; line-height: 16px; margin-top: 0; margin-right: 0; margin-bottom: 20px; margin-left: 20px; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-right: 0;\"> CONTACT US:<br> FRED FLINSTONE<br> BEDROCK GRAVEL, INC<br> 123 BENCH RD<br> BEDROCK, NY 11111 </p> </td> </tr> </table> <!-- Close second HTML table --> </td> </tr> </table> <!-- Close first HTML table -->  </body> </html>";

	private static final String JSON_BODY = "{ \"@odata.context\":\"https://graph.microsoft.com/v1.0/$metadata#users('94447c6e-ea4c-494c-a9ed-d905e366c5cb')/messages/$entity\", \"@odata.etag\":\"W/\\\"CQAAABYAAABK4UfANE/UR5clSilZtIuWAAC1vdti\\\"\", \"id\":\"AAMkADNlNYjSAAA=\", \"createdDateTime\":\"2017-07-22T01:53:56Z\", \"lastModifiedDateTime\":\"2017-07-22T01:53:57Z\", \"changeKey\":\"CQAAABYAAABK4UfANE/UR5clSilZtIuWAAC1vdti\", \"categories\":[ ], \"receivedDateTime\":\"2017-07-22T01:53:57Z\", \"sentDateTime\":\"2017-07-22T01:53:57Z\", \"hasAttachments\":false, \"internetMessageId\":\"<MWHPR1301MB@MWHPR1301MB.namprd13.prod.outlook.com>\", \"subject\":\"Did you see last night's game?\", \"bodyPreview\":\"They were awesome!\", \"importance\":\"low\", \"parentFolderId\":\"AAMkADNlNWAAAAAAEPAAA=\", \"conversationId\":\"AAQkADNlNFdXGBnqtY=\", \"isDeliveryReceiptRequested\":false, \"isReadReceiptRequested\":false, \"isRead\":true, \"isDraft\":true, \"webLink\":\"https://outlook.office365.com/owa/?ItemID=AAMkADNlNYjSAAA%3D&exvsurl=1&viewmodel=ReadMessageItem\", \"inferenceClassification\":\"focused\", \"body\":{ \"contentType\":\"html\", \"content\":\"<html>\\r\\n<head>\\r\\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\">\\r\\n<meta content=\\\"text/html; charset=us-ascii\\\">\\r\\n</head>\\r\\n<body>\\r\\nThey were <b>awesome</b>!\\r\\n</body>\\r\\n</html>\\r\\n\" }, \"toRecipients\":[ { \"emailAddress\":{ \"name\":\"Adele Vance\", \"address\":\"AdeleV@contoso.onmicrosoft.com\" } }, { \"emailAddress\":{ \"name\":\"Michael Vance\", \"address\":\"mike@contoso.onmicrosoft.com\" } } ], \"ccRecipients\":[{ \"emailAddress\":{ \"name\":\"Michael Vance\", \"address\":\"mike@contoso.onmicrosoft.com\" } } ], \"bccRecipients\":[ ], \"replyTo\":[{\"emailAddress\":{ \"name\":\"Michael Vance\", \"address\":\"john@contoso.onmicrosoft.com\"}}]}";

	private final Collection<String> toRecipients = Arrays.stream(new String[] { "brpe@semler.dk", "moiq@semler.dk" })
			.collect(Collectors.toSet());
	private final Collection<String> ccRecipients = Arrays.stream(new String[] { "moiq@semler.dk" })
			.collect(Collectors.toSet());
	private final Collection<String> bccRecipients = Arrays.stream(new String[] { "extsip@semler.dk" })
			.collect(Collectors.toSet());
	private final Collection<String> replyTos = Arrays.stream(new String[] { "brpe@semler.dk" })
			.collect(Collectors.toSet());

	final ObjectMapper mapper = new ObjectMapper();

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	@Disabled
	final void draftMailSerializationTest() throws Exception {
		Recipient toRecipient = Recipient.builder().addresses(toRecipients).build();
		Recipient ccRecipient = Recipient.builder().addresses(ccRecipients).build();
		Recipient bccRecipient = Recipient.builder().addresses(bccRecipients).build();
		Recipient replyTo = Recipient.builder().addresses(replyTos).build();
		Body body = new Body();
		body.setContentType(ContentType.HTML);
		body.setContent(HTML_BODY);

		Message draftMessage = DefaultMessage.builder().subject("TestMail").body(body).importance(Importance.NORMAL)
				.toRecipients(toRecipient).ccRecipients(ccRecipient).replyTo(replyTo).bccRecipients(bccRecipient)
				.build();

		final String json = mapper.defaultPrettyPrintingWriter().writeValueAsString(draftMessage);

		System.out.println(json);
	}

	@Test
	@Disabled
	final void draftMailWithAttachmentSerializationTest() throws Exception {
		Recipient toRecipient = Recipient.builder().addresses(toRecipients).build();
		Recipient ccRecipient = Recipient.builder().addresses(ccRecipients).build();
		Recipient bccRecipient = Recipient.builder().addresses(bccRecipients).build();
		Recipient replyTo = Recipient.builder().addresses(replyTos).build();

		Body body = new Body();
		body.setContentType(ContentType.HTML);
		body.setContent(HTML_BODY);

		Attachment pdfAttachment = FileAttachment.builder().name("pdf-test.pdf").contentId("pdf-test")
				.contentType("application/pdf")
				.contentBytes(Files.readAllBytes(
						Paths.get(getClass().getClassLoader().getResource("attachments/pdf-test.pdf").toURI())))
				.build();

		Attachment pictureAttachment = FileAttachment.builder().name("picture.png").contentId("picture")
				.contentType("image/png").contentBytes(Files.readAllBytes(
						Paths.get(getClass().getClassLoader().getResource("attachments/picture.png").toURI())))
				.build();

		Collection<Attachment> attachments = Arrays.asList(pdfAttachment, pictureAttachment);

		Message draftMessage = DefaultMessage.builder().subject("TestMail").body(body).importance(Importance.NORMAL)
				.toRecipients(toRecipient).ccRecipients(ccRecipient).replyTo(replyTo).bccRecipients(bccRecipient)
				.attachments(attachments).build();

		final String json = mapper.defaultPrettyPrintingWriter().writeValueAsString(draftMessage);

		System.out.println(json);
	}

	@Test
	@Disabled
	final void mailMessageDeserializationTest() throws Exception {
		Message mailMessage = mapper.readValue(JSON_BODY, DefaultMessage.class);

		Assertions.assertNotNull(mailMessage);
	}

	@Test
	final void sendMailDecoratorSerializationTest() throws Exception {
		Recipient toRecipient = Recipient.builder().addresses(toRecipients).build();
		Recipient ccRecipient = Recipient.builder().addresses(ccRecipients).build();
		Recipient bccRecipient = Recipient.builder().addresses(bccRecipients).build();
		Recipient replyTo = Recipient.builder().addresses(replyTos).build();
		Body body = new Body();
		body.setContentType(ContentType.HTML);
		body.setContent(HTML_BODY);

		Message message = DefaultMessage.builder().subject("TestMail").body(body).importance(Importance.NORMAL)
				.toRecipients(toRecipient).ccRecipients(ccRecipient).replyTo(replyTo).bccRecipients(bccRecipient)
				.build();

		Message sendMailMessageDecorated = new SendMailMessageDecorator(message, false);

		final String json = mapper.defaultPrettyPrintingWriter().writeValueAsString(sendMailMessageDecorated);

		System.out.println(json);

	}
}
