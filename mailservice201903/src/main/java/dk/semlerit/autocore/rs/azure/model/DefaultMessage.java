package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;
import java.util.Collection;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Representation of an Office 365 mail draft item. Often used as a request
 * class from the REST interfaces.
 * 
 * @author edbbrpe
 * @Version 1.0
 * @since 3.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultMessage implements Message, Serializable {
	private static final long serialVersionUID = -7932609532009859474L;

	@JsonProperty("id")
	private String id;
	@JsonProperty("subject")
	private String subject;
	@JsonProperty("body")
	private Body body;
	@JsonProperty("toRecipients")
	private Recipient toRecipients;
	@JsonProperty("ccRecipients")
	private Recipient ccRecipients;
	@JsonProperty("bccRecipients")
	private Recipient bccRecipients;
	@JsonProperty("replyTo")
	private Recipient replyTo;
	@JsonProperty("importance")
	private Importance importance;
	@JsonProperty("from")
	private Recipient from;
	private boolean read;
	@JsonProperty("conversationId")
	private String conversationId;
	@JsonProperty("webLink")
	private String webLink;
	@JsonProperty("attachments")
	private Collection<Attachment> attachments;

	@JsonProperty("isRead")
	public boolean isRead() {
		return read;
	}
}
