package dk.semlerit.autocore.rs.azure.map;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import dk.semlerit.autocore.rs.azure.model.Recipient;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class AzureMail {

	/**
	 * Custom JsonSerializer implementation which serializes a set of mail
	 * addresses to a JSON array of OWA/365 formatted mail addresses.
	 * 
	 * <pre>
	 *  [
	 *  	{
	 *   		"emailAddress": {
	 *       		"address": "fannyd@contoso.onmicrosoft.com"
	 *       	}
	 *   	},
	 *  	{
	 *   		"emailAddress": {
	 *       		"address": "manny@contoso.onmicrosoft.com"
	 *       	}
	 *   	}
	 *   ]
	 * </pre>
	 * 
	 * @author edbbrpe
	 * @version 1.0
	 * @since 3.0.0
	 */
	public static class RecipientSerializer extends JsonSerializer<Recipient> {

		@Override
		public void serialize(Recipient recipient, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {

			generator.writeStartArray();
			generator.writeStartObject();

			Iterator<String> it = recipient.getAddresses().iterator();
			while (it.hasNext()) {
				generator.writeObjectFieldStart("emailAddress");
				generator.writeStringField("address", it.next());
				generator.writeEndObject();
			}

			generator.writeEndObject();
			generator.writeEndArray();
		}
	}

	/**
	 * Custom JsonSerializer implementation which serializes a set of mail
	 * addresses to a JSON array of OWA/365 formatted mail addresses.
	 * 
	 * <pre>
	 *  [
	 *  	{
	 *   		"emailAddress": {
	 *       		"address": "fannyd@contoso.onmicrosoft.com"
	 *       	}
	 *   	},
	 *  	{
	 *   		"emailAddress": {
	 *       		"address": "manny@contoso.onmicrosoft.com"
	 *       	}
	 *   	}
	 *   ]
	 * </pre>
	 * 
	 * @author edbbrpe
	 * @version 1.0
	 * @since 3.0.0
	 */
	public static class RecipientDeserializer extends JsonDeserializer<Recipient> {

		@Override
		public Recipient deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			final ObjectCodec objectCodec = parser.getCodec();

			return Recipient.builder().addresses(new HashSet<>(Optional.ofNullable(objectCodec.readTree(parser))
					.orElseGet(() -> objectCodec.createArrayNode()).findValuesAsText("address"))).build();
		}

	}

	/**
	 * Custom JsonSerializer implementation which serializes a mail body to a
	 * JSON formatted OWA/365 body object.
	 * 
	 * <pre>
	 * "body": {
	 *  	"contentType": "html",
	 *     	"content": "&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;&lt;h1&gt;HelloWorld&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;"
	 *   }
	 * </pre>
	 * 
	 * @author edbbrpe
	 * @version 1.0
	 * @since 3.0.0
	 */
	public static class BodySerializer extends JsonSerializer<String> {

		@Override
		public void serialize(String bodyContent, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {

			generator.writeStartObject();
			generator.writeStringField("contentType", "html");
			generator.writeStringField("content", bodyContent);
			generator.writeEndObject();

		}
	}
}
