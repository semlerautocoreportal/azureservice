package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;
import org.codehaus.jackson.annotate.JsonTypeInfo;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@odata.type")
@JsonSubTypes({ @Type(value = FileAttachment.class, name = "#microsoft.graph.fileAttachment") })
public interface Attachment extends Serializable {

	/**
	 * Get the Odata type representing the Attachment type.
	 * 
	 * @return Odata.type
	 */
	@JsonIgnore
	String getOdataType();
}
