package dk.semlerit.autocore.rs.azure;

import javax.ws.rs.ClientErrorException;

import dk.semlerit.autocore.rs.azure.model.Attachment;
import dk.semlerit.autocore.rs.azure.model.Message;

/**
 * Office 365 Mail service
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface MailService {

	/**
	 * Create a new draft e-mail in the Office 365 mail of the user principal.
	 * 
	 * @param userPrincipalName
	 * @param mailDraft
	 * @return MailMessage instance updated with WebLink and ID
	 * @throws ClientErrorException
	 */
	Message createMailDraft(String userPrincipalName, Message draftMessage) throws ClientErrorException;

	/**
	 * Create and adds a mail attachment to a draft mail located in the offce
	 * 365 mail for the {@code userPrincipal}.
	 * 
	 * @param userPrincipalName
	 * @param messageId
	 * @param attachment
	 * @throws ClientErrorException
	 */
	void addAttachment(String userPrincipalName, String messageId, Attachment attachment) throws ClientErrorException;

	/**
	 * Send a mail message directly to a receiver from the Office 365 mail
	 * account of the {@code userPrincipal}.
	 * 
	 * @param userPrincipalName
	 * @param saveToSentItems
	 * @param message
	 * @throws ClientErrorException
	 */
	void sendMail(String userPrincipalName, boolean saveToSentItems, Message message) throws ClientErrorException;
}
