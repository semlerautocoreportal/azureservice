package dk.semlerit.autocore.rs.azure.model;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.NonNull;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class SendMailMessageDecorator extends MessageDecorator {

	private boolean saveToSentItems;

	public SendMailMessageDecorator(@NonNull Message message, boolean saveToSentItems) {
		super(message);
		this.saveToSentItems = saveToSentItems;
	}

	@JsonProperty("message")
	public Message getMessage() {
		return message;
	}

	@JsonProperty("saveToSentItems")
	public boolean isSaveToSentItems() {
		return saveToSentItems;
	}
}
