package dk.semlerit.autocore.rs.azure.model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Builder;
import lombok.Data;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileAttachment implements Attachment {
	private static final long serialVersionUID = 9107012575829215564L;

	@JsonProperty("name")
	private String name;
	@JsonProperty("contentType")
	private String contentType;
	@JsonProperty("contentBytes")
	private byte[] contentBytes;
	@JsonProperty("contentId")
	private String contentId;
	private boolean inline;

	@JsonCreator
	public FileAttachment() {
	}

	@Builder
	public FileAttachment(String name, String contentType, byte[] contentBytes, String contentId, boolean inline) {
		super();
		this.name = name;
		this.contentType = contentType;
		this.contentBytes = contentBytes;
		this.contentId = contentId;
		this.inline = inline;
	}

	@JsonProperty("isInline")
	public boolean isInline() {
		return inline;
	}

	@JsonProperty("@odata.type")
	@Override
	public String getOdataType() {
		return "#microsoft.graph.fileAttachment";
	}
}
