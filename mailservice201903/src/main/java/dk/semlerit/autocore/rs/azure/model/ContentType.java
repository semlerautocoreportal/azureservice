package dk.semlerit.autocore.rs.azure.model;

import java.util.Objects;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public enum ContentType {
	TEXT,
	HTML;

	@JsonCreator
	public static ContentType fromValue(String value) {
		return Objects.isNull(value) ? null : ContentType.valueOf(value.toUpperCase());
	}

	@JsonValue
	public String toValue() {
		return name().toLowerCase();
	}
}
