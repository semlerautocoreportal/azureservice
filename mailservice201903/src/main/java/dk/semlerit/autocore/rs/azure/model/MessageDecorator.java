package dk.semlerit.autocore.rs.azure.model;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@JsonIgnoreProperties({ "id", "subject", "body", "toRecipients", "ccRecipients", "bccRecipients", "replyTo",
		"importance", "from", "read", "conversationId", "webLink", "attachments" })
abstract class MessageDecorator implements Message {
	protected Message message;

	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public MessageDecorator(Message message) {
		super();
		this.message = message;
	}

	@Override
	public String getId() {
		return message.getId();
	}

	@Override
	public String getSubject() {
		return message.getSubject();
	}

	@Override
	public Body getBody() {
		return message.getBody();
	}

	@Override
	public Recipient getToRecipients() {
		return message.getToRecipients();
	}

	@Override
	public Recipient getCcRecipients() {
		return message.getCcRecipients();
	}

	@Override
	public Recipient getBccRecipients() {
		return message.getBccRecipients();
	}

	@Override
	public Recipient getReplyTo() {
		return message.getReplyTo();
	}

	@Override
	public Importance getImportance() {
		return message.getImportance();
	}

	@Override
	public Recipient getFrom() {
		return message.getFrom();
	}

	@Override
	public boolean isRead() {
		return message.isRead();
	}

	@Override
	public String getConversationId() {
		return message.getConversationId();
	}

	@Override
	public String getWebLink() {
		return message.getWebLink();
	}

	@Override
	public Collection<Attachment> getAttachments() {
		return message.getAttachments();
	}
}
