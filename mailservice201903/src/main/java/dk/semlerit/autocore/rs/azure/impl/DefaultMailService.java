package dk.semlerit.autocore.rs.azure.impl;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import dk.semlerit.autocore.rs.azure.MailService;
import dk.semlerit.autocore.rs.azure.core.AzureClientSupport;
import dk.semlerit.autocore.rs.azure.model.Attachment;
import dk.semlerit.autocore.rs.azure.model.DefaultMessage;
import dk.semlerit.autocore.rs.azure.model.Message;
import dk.semlerit.autocore.rs.azure.model.SendMailMessageDecorator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @Since 3.0.0
 */
@ApplicationScoped
@Slf4j
public class DefaultMailService implements MailService {

	private static final String GRAPH_RESOURCE = "https://graph.microsoft.com";
	private static final String GRAPH_BASE_URI = String.join("/", GRAPH_RESOURCE, "beta");

	@Inject
	private AzureClientSupport azureClientSupport;

	private Client client;

	/**
	 * @throws NullPointerException
	 *             if userPrincipalName or mailDraft is {@literal null}
	 */
	@Override
	public Message createMailDraft(@NonNull String userPrincipalName, @NonNull Message draftMessage)
			throws ClientErrorException {

		final URI uri = UriBuilder.fromUri(GRAPH_BASE_URI).path("users").path(userPrincipalName).path("messages")
				.build();

		Instant start = Instant.now();

		Message message = client.target(uri).request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION,
						String.format("Bearer %s", azureClientSupport.getAccessToken(GRAPH_RESOURCE)))
				.post(Entity.json(draftMessage)).readEntity(DefaultMessage.class);

		Instant end = Instant.now();
		log.debug("Remote rest call took: {} milliseconds.", Duration.between(start, end));

		return message;
	}

	/**
	 * @throws NullPointerException
	 *             if userPrincipalName, messageId or attachment is
	 *             {@literal null} *
	 */
	@Override
	public void addAttachment(@NonNull String userPrincipalName, @NonNull String messageId,
			@NonNull Attachment attachment) throws ClientErrorException {

		final URI uri = UriBuilder.fromUri(GRAPH_BASE_URI).path("users").path(userPrincipalName).path("messages")
				.path(messageId).path("attachments").build();

		Instant start = Instant.now();

		azureClientSupport.handleResponse(client.target(uri).request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION,
						String.format("Bearer %s", azureClientSupport.getAccessToken(GRAPH_RESOURCE)))
				.post(Entity.json(attachment)));

		Instant end = Instant.now();
		log.debug("Remote rest call took: {} milliseconds.", Duration.between(start, end));
	}

	/**
	 * @throws NullPointerException
	 *             if userPrincipalName or message is {@literal null} *
	 */
	@Override
	public void sendMail(@NonNull String userPrincipalName, boolean saveToSentItems, @NonNull Message message)
			throws ClientErrorException {

		final URI uri = UriBuilder.fromUri(GRAPH_BASE_URI).path("users").path(userPrincipalName).path("sendMail")
				.build();

		Message decoratedMessage = new SendMailMessageDecorator(message, saveToSentItems);

		Instant start = Instant.now();

		azureClientSupport.handleResponse(client.target(uri).request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION,
						String.format("Bearer %s", azureClientSupport.getAccessToken(GRAPH_RESOURCE)))
				.post(Entity.json(decoratedMessage)));

		Instant end = Instant.now();
		log.debug("Remote rest call took: {} milliseconds.", Duration.between(start, end));

	}

	@PostConstruct
	public void init() {
		this.client = ClientBuilder.newClient();
	}

	@PreDestroy
	public void destroy() {
		if (Objects.nonNull(client)) {
			client.close();
		}
		client = null;
	}
}
