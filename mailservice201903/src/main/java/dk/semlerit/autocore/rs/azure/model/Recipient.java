package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import dk.semlerit.autocore.rs.azure.map.AzureMail;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
@JsonSerialize(using = AzureMail.RecipientSerializer.class)
@JsonDeserialize(using = AzureMail.RecipientDeserializer.class)
public class Recipient implements Serializable {
	private static final long serialVersionUID = -7150203228160336664L;

	@Builder.Default
	private Collection<String> addresses = new HashSet<>();
}
