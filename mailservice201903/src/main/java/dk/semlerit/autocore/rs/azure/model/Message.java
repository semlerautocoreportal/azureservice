package dk.semlerit.autocore.rs.azure.model;

import java.util.Collection;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface Message {

	/**
	 * 
	 * @return
	 */
	String getId();

	/**
	 * 
	 * @return
	 */
	String getSubject();

	/**
	 * 
	 * @return
	 */
	Body getBody();

	/**
	 * 
	 * @return
	 */
	Recipient getToRecipients();

	/**
	 * 
	 * @return
	 */
	Recipient getCcRecipients();

	/**
	 * 
	 * @return
	 */
	Recipient getBccRecipients();

	/**
	 * 
	 * @return
	 */
	Recipient getReplyTo();

	/**
	 * 
	 * @return
	 */
	Importance getImportance();

	/**
	 * 
	 * @return
	 */
	Recipient getFrom();

	/**
	 * 
	 * @return
	 */
	boolean isRead();

	/**
	 * 
	 * @return
	 */
	String getConversationId();

	/**
	 * 
	 * @return
	 */
	String getWebLink();

	/**
	 * 
	 * @return
	 */
	Collection<Attachment> getAttachments();

}
