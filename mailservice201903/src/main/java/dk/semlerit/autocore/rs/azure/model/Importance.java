package dk.semlerit.autocore.rs.azure.model;

import java.util.Objects;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

/**
 * Represents the Importance/Priority element.
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public enum Importance {
	LOW,
	NORMAL,
	HIGH;

	@JsonCreator
	public static Importance fromValue(String value) {
		return Objects.isNull(value) ? null : Importance.valueOf(value.toUpperCase());
	}

	@JsonValue
	public String toValue() {
		return name().toLowerCase();
	}
}
