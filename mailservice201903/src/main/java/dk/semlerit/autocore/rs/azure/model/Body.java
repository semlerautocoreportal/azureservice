package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Data;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Data
public class Body implements Serializable {
	private static final long serialVersionUID = 295223446635997728L;

	@JsonProperty("content")
	private String content;
	@JsonProperty("contentType")
	private ContentType contentType;
}
