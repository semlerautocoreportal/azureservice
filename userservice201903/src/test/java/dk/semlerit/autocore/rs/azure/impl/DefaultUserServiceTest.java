package dk.semlerit.autocore.rs.azure.impl;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.weld.junit5.EnableWeld;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import dk.semlerit.autocore.rs.azure.UserService;
import dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport;
import dk.semlerit.autocore.rs.azure.model.ADUser;
import dk.semlerit.autocore.rs.azure.model.ADUserGroup;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@EnableWeld
@ExtendWith(MockitoExtension.class)
class DefaultUserServiceTest {

	@WeldSetup
	public WeldInitiator weld = WeldInitiator
			.from(DefaultUserService.class, DefaultAzureClientSupport.class, ObjectMapper.class)
			.activate(ApplicationScoped.class).build();

	@Inject
	private UserService userService;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * Test method for {@link dk.semlerit.autocore.rs.azure.impl.DefaultUserService#retrieveUser(java.lang.String)}.
	 */
	@Test
	final void testRetrieveUser() {
		final String userPrincipalName = "thki@semler.dk";

		ADUser user = userService.retrieveUser(userPrincipalName);

		Assertions.assertNotNull(user);
	}

	@Test
	final void testRetrieveUserGroups() {
		final String userPrincipalName = "thki@semler.dk";

		List<ADUserGroup> groups = userService.retrieveUserGroups(userPrincipalName);

		Assertions.assertNotNull(groups);
	}
}
