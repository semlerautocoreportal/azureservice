package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author edbmi
 * @version 1.0
 * @since 3.0.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ADUserGroup implements Serializable {
	private static final long serialVersionUID = 9084037806491476924L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("displayName")
	private String displayName;

	@JsonProperty("description")
	private String description;

	@JsonProperty("securityIdentifier")
	private String securityIdentifier;

}
