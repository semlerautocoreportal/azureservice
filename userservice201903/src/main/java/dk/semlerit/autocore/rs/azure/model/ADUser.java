package dk.semlerit.autocore.rs.azure.model;

import java.io.Serializable;
import java.security.Principal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ADUser implements Principal, Serializable {
	private static final long serialVersionUID = 918690877723993552L;

	@JsonProperty("id")
	private String uid;
	@JsonProperty("userPrincipalName")
	private String userPrincipalName;
	@JsonProperty("mail")
	private String mail;
	@JsonProperty("mailNickname")
	private String mailNickname;
	@JsonProperty("givenName")
	private String firstName;
	@JsonProperty("surname")
	private String lastName;
	@JsonProperty("displayName")
	private String displayName;
	@JsonProperty("jobTitle")
	private String jobTitle;
	@JsonProperty("streetAddress")
	private String address;
	@JsonProperty("city")
	private String city;
	@JsonProperty("postalCode")
	private String postalCode;
	@JsonProperty("country")
	private String country;
	@JsonProperty("telephoneNumber")
	private String telephone;
	@JsonProperty("mobile")
	private String mobile;
	@JsonProperty("accountEnabled")
	private boolean accountEnabled;
	@JsonProperty("userType")
	private String accountType;

	@Override
	public String getName() {
		return userPrincipalName;
	}
}
