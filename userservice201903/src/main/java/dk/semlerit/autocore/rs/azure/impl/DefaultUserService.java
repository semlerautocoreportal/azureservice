package dk.semlerit.autocore.rs.azure.impl;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.UriBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import dk.semlerit.autocore.rs.azure.UserService;
import dk.semlerit.autocore.rs.azure.core.AzureClientSupport;
import dk.semlerit.autocore.rs.azure.model.ADUser;
import dk.semlerit.autocore.rs.azure.model.ADUserGroup;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 
 * @author edbbrpe
 * @author extmbi
 * @version 1.0
 * @since 3.0.0
 */
@ApplicationScoped
@Slf4j
public class DefaultUserService implements UserService {
	private static final String GRAPH_RESOURCE = "https://graph.microsoft.com";
	private static final String GRAPH_BASE_URI = String.join("/", GRAPH_RESOURCE, "beta");
	private final String[] FIELD_SELECTIONS = { "id", "mail", "mailNickname", "givenName", "surname", "displayName",
			"jobTitle", "streetAddress", "city", "postalCode", "country", "telephoneNumber", "mobile", "accountEnabled",
			"userType" };

	private final String[] FIELD_SELECTIONS_USER_GROUPS = { "id", "displayName", "description", "securityIdentifier" };

	@Inject
	private AzureClientSupport azureClientSupport;

	private Client client;

	/**
	 * @throws NullPointerException
	 *             if userPrincipal name is {@literal null}
	 */
	@Override
	@Transactional
	public ADUser retrieveUser(@NotNull String userPrincipalName) throws ClientErrorException {

		final URI uri = UriBuilder.fromUri(GRAPH_BASE_URI).path("users")
				.path(Objects.requireNonNull(userPrincipalName, "userPrincipal must not be null"))
				.queryParam("$select", String.join(",", FIELD_SELECTIONS)).build();

		Instant start = Instant.now();
		ADUser user = client.target(uri).request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION,
						String.format("Bearer %s", azureClientSupport.getAccessToken(GRAPH_RESOURCE)))
				.get(ADUser.class);
		Instant end = Instant.now();
		log.debug("Remote rest call took: {} milliseconds.", Duration.between(start, end));

		return user;
	}

	@PostConstruct
	public void init() {
		this.client = ClientBuilder.newClient();
	}

	@PreDestroy
	public void destroy() {
		if (Objects.nonNull(client)) {
			client.close();
		}
		client = null;
	}

	@Override
	@Transactional
	public List<ADUserGroup> retrieveUserGroups(final String userPrincipalName) throws ClientErrorException {
		final URI uri = UriBuilder.fromUri(GRAPH_BASE_URI).path("users")
				.path(Objects.requireNonNull(userPrincipalName, "userPrincipal must not be null")).path("memberOf")
				.queryParam("$select", String.join(",", FIELD_SELECTIONS_USER_GROUPS)).build();

		ObjectMapper objectMapper = new ObjectMapper();
		Instant start = Instant.now();
		List<ADUserGroup> groups = null;
		MultivaluedHashMap<String, Object> headerValues = new MultivaluedHashMap<>();
		headerValues.add(HttpHeaders.AUTHORIZATION,
				String.format("Bearer %s", azureClientSupport.getAccessToken(GRAPH_RESOURCE)));
		headerValues.add(HttpHeaders.ACCEPT, "application/json;odata.metadata=none");

		final String rsResponse = client.target(uri).request(MediaType.APPLICATION_JSON).headers(headerValues)
				.get(String.class);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);

		try {
			final JsonNode jsonNodeUserGroupArray = objectMapper.readTree(rsResponse).get("value");
			groups = objectMapper.readValue(jsonNodeUserGroupArray.toString(),
					new TypeReference<Collection<ADUserGroup>>() {
					});
		} catch (IOException e) {
			log.error(e.getLocalizedMessage(), e);
		}

		Instant end = Instant.now();
		log.debug("Remote rest call took: {} milliseconds.", Duration.between(start, end));

		return groups;
	}

}
