package dk.semlerit.autocore.rs.azure;

import java.util.List;

import javax.ws.rs.ClientErrorException;

import dk.semlerit.autocore.rs.azure.model.ADUser;
import dk.semlerit.autocore.rs.azure.model.ADUserGroup;

/**
 * 
 * 
 * @author edbbrpe
 * @author edbmi
 * @version 1.1
 * @since 3.0.0
 */
public interface UserService {

	/**
	 * Retrieve user from Azure AD by user principal name.
	 * 
	 * @param userPrincipalName
	 * @return an ADUser
	 * @throws ClientErrorException
	 */
	ADUser retrieveUser(String userPrincipalName) throws ClientErrorException;

	/**
	 * Retrieve groups for a user in AzureAD/Office365 by user principal name (UPN) UPN is usually the email address of
	 * the user requested.
	 * 
	 * @param retrieveUserGroupRequestTO
	 * @return list of ADUserGroup
	 * @throws ClientErrorException
	 */

	List<ADUserGroup> retrieveUserGroups(String userPrincipalName) throws ClientErrorException;

}
