package dk.semlerit.autocore.rs.azure.core;

import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.ACCESS_TOKEN_CB;
import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.CB_FAILURE_RATE_THRESHOLD;
import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.CB_MINIMUM_NUMBER_OF_CALLS;
import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.CB_PERMITTED_NUMBER_OF_CALLS_IN_HALF_OPEN_STATE;
import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.CB_SLIDING_WINDOW_SIZE;
import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.CB_SLIDING_WINDOW_TYPE;
import static dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport.CB_SLOW_CALL_RATE_THRESHOLD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Duration;

import javax.ws.rs.InternalServerErrorException;

import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthClientResponseFactory;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import dk.semlerit.autocore.rs.azure.core.impl.DefaultAzureClientSupport;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreaker.State;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;

@ExtendWith(MockitoExtension.class)
class DefaultAzureClientSupportTest {
	private static final Duration CB_SLOW_CALL_DURATION_THRESHOLD = Duration.ofSeconds(1);
	private static final Duration CB_DURATION_IN_OPEN_STATE = Duration.ofSeconds(1);

	private static final String SUCCESSFUL_ACCESS_TOKEN = "valid_access_token";
	private static final String SUCCESSFUL_TOKEN_RESPONSE = "{\"access_token\":\"" + SUCCESSFUL_ACCESS_TOKEN
			+ "\",\"expires_in\":2419200}";

	@InjectMocks
	DefaultAzureClientSupport defaultAzureClientSupport;
	@Mock
	OAuthClient oAuthClient;
	CircuitBreaker circuitBreaker;

	@BeforeEach
	public void setup() {
		final CircuitBreakerConfig config = CircuitBreakerConfig.custom().slidingWindowType(CB_SLIDING_WINDOW_TYPE)
				.minimumNumberOfCalls(CB_MINIMUM_NUMBER_OF_CALLS).slidingWindowSize(CB_SLIDING_WINDOW_SIZE)
				.failureRateThreshold(CB_FAILURE_RATE_THRESHOLD).slowCallRateThreshold(CB_SLOW_CALL_RATE_THRESHOLD)
				.slowCallDurationThreshold(CB_SLOW_CALL_DURATION_THRESHOLD)
				.waitDurationInOpenState(CB_DURATION_IN_OPEN_STATE)
				.permittedNumberOfCallsInHalfOpenState(CB_PERMITTED_NUMBER_OF_CALLS_IN_HALF_OPEN_STATE).build();

		final CircuitBreakerRegistry registry = CircuitBreakerRegistry.of(config);
		circuitBreaker = registry.circuitBreaker(ACCESS_TOKEN_CB);
		defaultAzureClientSupport.setCircuitBreaker(circuitBreaker);
	}

	@Test
	void testOAuthClientThrowsException() throws Throwable {
		when(oAuthClient.accessToken(any(OAuthClientRequest.class))).thenThrow(new OAuthSystemException());

		// Try to get access token 6 times
		for (int i = 0; i < 6; i++) {
			assertThrows(InternalServerErrorException.class, () -> {
				defaultAzureClientSupport.getAccessToken("");
			});
		}

		// Only 4 request should have been made to oAuthClient
		verify(oAuthClient, Mockito.times(4)).accessToken(any(OAuthClientRequest.class));
	}

	@Test
	void testOAuthClientLongResponseTime() throws Throwable {
		// Delay oAuthClient request 3 seconds
		doAnswer(AdditionalAnswers.answersWithDelay(1500, invocationOnMock -> {
			final OAuthJSONAccessTokenResponse responseToken = OAuthClientResponseFactory.createCustomResponse(
					SUCCESSFUL_TOKEN_RESPONSE, "application/json", 200, null, OAuthJSONAccessTokenResponse.class);
			return responseToken;
		})).when(oAuthClient).accessToken(any(OAuthClientRequest.class));

		// Circuit breaker state on first three request should be CLOSED
		for (int i = 0; i < 3; i++) {
			assertEquals(SUCCESSFUL_ACCESS_TOKEN, defaultAzureClientSupport.getAccessToken(Math.random() + ""));
			assertEquals(State.CLOSED, circuitBreaker.getState());
		}

		// After the 4th request the circuit breaker should change state to OPEN
		assertEquals(SUCCESSFUL_ACCESS_TOKEN, defaultAzureClientSupport.getAccessToken(Math.random() + ""));
		assertEquals(State.OPEN, circuitBreaker.getState());

		// After the 5th request the circuit breaker should still be OPEN and response should be from the fallback
		// method.
		assertThrows(InternalServerErrorException.class, () -> {
			defaultAzureClientSupport.getAccessToken(Math.random() + "");
		});
		assertEquals(State.OPEN, circuitBreaker.getState());

		Thread.sleep(1500);

		when(oAuthClient.accessToken(any(OAuthClientRequest.class)))
				.thenReturn(OAuthClientResponseFactory.createCustomResponse(SUCCESSFUL_TOKEN_RESPONSE,
						"application/json", 200, null, OAuthJSONAccessTokenResponse.class));

		assertEquals(SUCCESSFUL_ACCESS_TOKEN, defaultAzureClientSupport.getAccessToken(Math.random() + ""));
		assertEquals(State.HALF_OPEN, circuitBreaker.getState());

		assertEquals(SUCCESSFUL_ACCESS_TOKEN, defaultAzureClientSupport.getAccessToken(Math.random() + ""));
		assertEquals(State.CLOSED, circuitBreaker.getState());

		// 6 request should have been made to oAuthClient
		verify(oAuthClient, Mockito.times(6)).accessToken(Mockito.any(OAuthClientRequest.class));
	}
}