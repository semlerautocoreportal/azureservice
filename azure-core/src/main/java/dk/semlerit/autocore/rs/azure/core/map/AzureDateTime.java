package dk.semlerit.autocore.rs.azure.core.map;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class AzureDateTime {

	/**
	 * 
	 * 
	 * @author edbbrpe
	 * @version 1.0
	 * @since 3.0.0
	 */
	public static class UTCDateTimeDeserializer extends JsonDeserializer<Date> {

		@Override
		public Date deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			final JsonNode nodeRoot = parser.getCodec().readTree(parser);

			String dateTimeTextValue = Optional.ofNullable(nodeRoot.get("dateTime")).orElseThrow(
					() -> new JsonParseException("Could not parse Azure UTC dateTime", parser.getCurrentLocation()))
					.getTextValue();
			TimeZone timeZone = TimeZone.getTimeZone(Optional.ofNullable(nodeRoot.get("timeZone")).orElseThrow(
					() -> new JsonParseException("Could not parse Azuer UTC timeZone", parser.getCurrentLocation()))
					.getTextValue());

			ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTimeTextValue,
					DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS").withZone(timeZone.toZoneId()));
			ZonedDateTime localZonedDateTime = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault());

			return Date.from(localZonedDateTime.toInstant());
		}
	}

	/**
	 * 
	 * 
	 * @author edbbrpe
	 * @version 1.0
	 * @since 3.0.0
	 */
	public static class UTCDateTimeSerializer extends JsonSerializer<Date> {

		@Override
		public void serialize(Date date, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			// FIX needs fixing to write a proper Azure Json object.

			if (Objects.isNull(date)) {
				generator.writeNull();
				return;
			}

			generator.writeString(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
					.format(ZonedDateTime.ofInstant(date.toInstant(), ZoneOffset.ofOffset("UTC", ZoneOffset.UTC))));
		}
	}
}
