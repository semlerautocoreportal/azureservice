package dk.semlerit.autocore.rs.azure.core;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface AzureClientSupport {

	/**
	 * Get an access token from Token endpoint.
	 * 
	 * @return accessToken or {@literal null}
	 */
	String getAccessToken(String resource);

	/**
	 * Handle REST response.
	 * 
	 * @param response
	 * @throws ClientErrorException
	 */
	void handleResponse(Response response) throws ClientErrorException;
}
