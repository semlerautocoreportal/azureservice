package dk.semlerit.autocore.rs.azure.core.impl;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.Response;

import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;

import dk.semlerit.autocore.rs.azure.core.AzureClientSupport;
import dk.semlerit.autocore.web.exception.CacheException;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig.SlidingWindowType;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.decorators.Decorators;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@ApplicationScoped
@Slf4j
public class DefaultAzureClientSupport implements AzureClientSupport {
	public static final String AZURE_API_TOKEN = "azure.api.token";
	public static final String AZURE_API_TOKEN_EXPIRY = "azure.api.token.expiry";

	private static final String AZURE_TENANT_ID = "6ee0d2f5-899d-48c8-b2a3-33061f5665bc";
	private static final String AZURE_TOKEN_ENDPOINT = String
			.format("https://login.microsoftonline.com/%s/oauth2/token", AZURE_TENANT_ID);
	private static final String AZURE_TENANT_PRINCIPAL = "5e9475fa-f229-43e2-a3fa-8d7c57b8b5d6";
	private static final String AZURE_TENANT_CREDENTIALS = "kvXRodatrVVk4wkQjpVud5i";

	public static final String ACCESS_TOKEN_CB = "accessTokenCB";
	public static final SlidingWindowType CB_SLIDING_WINDOW_TYPE = SlidingWindowType.COUNT_BASED;
	public static final int CB_MINIMUM_NUMBER_OF_CALLS = 4;
	public static final int CB_SLIDING_WINDOW_SIZE = 4;
	public static final float CB_FAILURE_RATE_THRESHOLD = 50;
	public static final float CB_SLOW_CALL_RATE_THRESHOLD = 50;
	public static final Duration CB_SLOW_CALL_DURATION_THRESHOLD = Duration.ofSeconds(2);
	public static final Duration CB_DURATION_IN_OPEN_STATE = Duration.ofSeconds(30);
	public static final int CB_PERMITTED_NUMBER_OF_CALLS_IN_HALF_OPEN_STATE = 2;

	private final Map<String, TokenContainer> tokenPool = new HashMap<>();

	@Setter
	private CircuitBreaker circuitBreaker;

	@Inject
	private OAuthClient oAuthClient;

	/**
	 * Return an access token for Azure services.
	 * 
	 * @return a bearer access token for Azure services
	 * @throws NullPointerException
	 *             if resource is {@literal null}
	 */
	@Override
	@Transactional
	public String getAccessToken(@NotNull final String resource) {
		final String tokenKey = String.join(".", AZURE_API_TOKEN, resource);

		final Supplier<String> accessTokenSupplier = () -> Optional.ofNullable(getTokenContainer(tokenKey))
				.orElseGet(new Supplier<TokenContainer>() {
					@Override
					public TokenContainer get() {
						TokenContainer tokenContainer = getTokenContainer(tokenKey);

						if (Objects.nonNull(tokenContainer)) {
							return tokenContainer;
						}

						try {
							final OAuthClientRequest oAuthClientRequest = OAuthClientRequest
									.tokenLocation(AZURE_TOKEN_ENDPOINT).setGrantType(GrantType.CLIENT_CREDENTIALS)
									.setClientId(AZURE_TENANT_PRINCIPAL).setClientSecret(AZURE_TENANT_CREDENTIALS)
									.setParameter("resource", resource).buildBodyMessage();

							final OAuthJSONAccessTokenResponse response = oAuthClient.accessToken(oAuthClientRequest);

							tokenContainer = TokenContainer.builder().token(response.getAccessToken())
									.expiryMs(System.currentTimeMillis() + response.getExpiresIn()).build();
							tokenPool.put(tokenKey, tokenContainer);

							return tokenContainer;
						} catch (CacheException | OAuthSystemException | OAuthProblemException e) {
							log.error("Could not obtain a valid token from Azure endpoint.", e);

							throw new InternalServerErrorException(
									"Could not obtain a valid token from Azure endpoint.", e);
						}
					}
				}).getToken();

		final Supplier<String> decoratedAccessTokenSupplier = Decorators.ofSupplier(accessTokenSupplier)
				.withCircuitBreaker(circuitBreaker).withFallback(e -> {
					log.error("Circuit breaker fallback method activated", e);

					throw new InternalServerErrorException("Could not retrieve access token (Circuit breaker)");
				}).decorate();

		return decoratedAccessTokenSupplier.get();
	}

	/**
	 * Default handling of response status codes.
	 * 
	 * @param response
	 * @throws ClientErrorException
	 */
	@Override
	@Transactional
	public void handleResponse(@NonNull final Response response) throws ClientErrorException {
		switch (response.getStatusInfo().getFamily()) {
			case CLIENT_ERROR:
				throw new BadRequestException(response.readEntity(String.class), response);
			case SERVER_ERROR:
				throw new InternalServerErrorException(response.readEntity(String.class), response);
			case SUCCESSFUL:
				break;
			default:
				break;
		}
	}

	@PostConstruct
	private void init() {
		final CircuitBreakerConfig config = CircuitBreakerConfig.custom().slidingWindowType(CB_SLIDING_WINDOW_TYPE)
				.minimumNumberOfCalls(CB_MINIMUM_NUMBER_OF_CALLS).slidingWindowSize(CB_SLIDING_WINDOW_SIZE)
				.failureRateThreshold(CB_FAILURE_RATE_THRESHOLD).slowCallRateThreshold(CB_SLOW_CALL_RATE_THRESHOLD)
				.slowCallDurationThreshold(CB_SLOW_CALL_DURATION_THRESHOLD)
				.waitDurationInOpenState(CB_DURATION_IN_OPEN_STATE)
				.permittedNumberOfCallsInHalfOpenState(CB_PERMITTED_NUMBER_OF_CALLS_IN_HALF_OPEN_STATE).build();

		final CircuitBreakerRegistry registry = CircuitBreakerRegistry.of(config);

		circuitBreaker = registry.circuitBreaker(ACCESS_TOKEN_CB);
	}

	@Produces
	private OAuthClient createOAuthClient() {
		return new OAuthClient(new URLConnectionClient());
	}

	// ~ Internal functions ====================================================

	private TokenContainer getTokenContainer(@NotNull final String key) {
		final TokenContainer tokenContainer = tokenPool.get(key);

		if (Objects.isNull(tokenContainer)) {
			return null;
		}

		return tokenContainer.isExpired() ? null : tokenContainer;
	}

	// ~ Internal classes ======================================================

	/**
	 * Internal class used as a token container.
	 */
	@Builder
	@Getter
	@Setter
	@ToString
	@EqualsAndHashCode
	static class TokenContainer implements Serializable {
		private static final long serialVersionUID = -4786544420208836544L;

		private String token;
		private Long expiryMs;

		public boolean isExpired() {
			return expiryMs <= System.currentTimeMillis();
		}
	}
}
